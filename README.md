# Desafio - Sistema de Entregas

## Descrição

Objetivo do projeto é encontrar o menor custo para entrega com base nos dados informados.

## 1 - Persistência dos dados

Arquivo (.json) salvo na raiz do repositório

## 2 - Rodando a Aplicação
Para rodar a aplicação utilizar o comando Maven: `mvn spring-boot:run`

## 3 - Consumindo a Aplicação

## 3.1 - Rotas
Para consumir os Serviços de Rotas utiliza-se o EndPoit abaixo:

       Method: POST localhost:8080/rota  informando o JSON
       {  
         "origem": "String",
         "destino": `String",
         "distancia": "Double"
       }

## 3.2 - Logistica
Para consumir os Serviços de Logistica utiliza-se o EndPoit abaixo:

      Method: POST localhost:8080/logistica/menorCusto  informando o JSON
       {  
         "origem": "String",
         "destino": "String",
         "autonomia": "Integer",
         "valorLitro": "Double"
       }

     Retornará o Seguinte JSON:
     {
        "rota": "String",
        "custo": "Double"
      }
