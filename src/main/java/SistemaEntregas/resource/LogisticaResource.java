package SistemaEntregas.resource;

import SistemaEntregas.model.FiltroLogisticaDTO;
import SistemaEntregas.model.MenorRotaDTO;
import SistemaEntregas.service.LogisticaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/logistica")
public class LogisticaResource {

    @Autowired
    private LogisticaService logisticaService;

    @PostMapping("/menorCusto")
    public ResponseEntity<MenorRotaDTO> buscarMenorCusto(@RequestBody FiltroLogisticaDTO filtro) throws Exception {

        logisticaService.calcularRotas(filtro);
        return new ResponseEntity(logisticaService.calcularRotas(filtro), HttpStatus.OK);
    }


}
