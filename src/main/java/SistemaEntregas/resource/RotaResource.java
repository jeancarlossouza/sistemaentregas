package SistemaEntregas.resource;

import SistemaEntregas.model.RotaDTO;
import SistemaEntregas.service.RotaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/rota")
public class RotaResource {

    @Autowired
    private RotaService rotaService;

    @PostMapping
    public ResponseEntity<List<RotaDTO>> adicionar(@RequestBody List<RotaDTO> rotaList) throws Exception {

        return ResponseEntity.ok(rotaService.salvar(rotaList));
    }


}
