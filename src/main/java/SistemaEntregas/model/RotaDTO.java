package SistemaEntregas.model;

import java.io.Serializable;

public class RotaDTO implements Serializable {

    private String origem;
    private String destino;
    private Double distancia;

    public String getOrigem() {
        return origem;
    }

    public RotaDTO setOrigem(String origem) {
        this.origem = origem;
        return this;
    }

    public String getDestino() {
        return destino;
    }

    public RotaDTO setDestino(String destino) {
        this.destino = destino;
        return this;
    }

    public Double getDistancia() {
        return distancia;
    }

    public RotaDTO setDistancia(Double distancia) {
        this.distancia = distancia;
        return this;
    }
}
