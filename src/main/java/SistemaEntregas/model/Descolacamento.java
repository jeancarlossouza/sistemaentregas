package SistemaEntregas.model;

public class Descolacamento {

    public final Vertice destino;
    public final double distancia;

    public Descolacamento(Vertice destino, double distancia) {
        this.destino = destino;
        this.distancia = distancia;
    }
}