package SistemaEntregas.model;

import java.util.List;

public class Vertice implements Comparable<Vertice> {

    public final String nome;
    public List<Descolacamento> adjacentes;
    public double distancia = Double.POSITIVE_INFINITY;
    public Vertice previous;

    public Vertice(String argName) {
        nome = argName;
    }

    public String toString() {
        return nome;
    }

    public int compareTo(Vertice other) {
        return Double.compare(distancia, other.distancia);
    }

}
