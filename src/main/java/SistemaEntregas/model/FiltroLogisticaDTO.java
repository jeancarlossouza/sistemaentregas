package SistemaEntregas.model;

public class FiltroLogisticaDTO {

    private String origem;
    private String destino;
    private Double autonomia;
    private Double valorLitro;

    public String getOrigem() {
        return origem;
    }

    public FiltroLogisticaDTO setOrigem(String origem) {
        this.origem = origem;
        return this;
    }

    public String getDestino() {
        return destino;
    }

    public FiltroLogisticaDTO setDestino(String destino) {
        this.destino = destino;
        return this;
    }

    public Double getAutonomia() {
        return autonomia;
    }

    public FiltroLogisticaDTO setAutonomia(Double autonomia) {
        this.autonomia = autonomia;
        return this;
    }

    public Double getValorLitro() {
        return valorLitro;
    }

    public FiltroLogisticaDTO setValorLitro(Double valorLitro) {
        this.valorLitro = valorLitro;
        return this;
    }
}
