package SistemaEntregas.model;

import java.io.Serializable;

public class MenorRotaDTO implements Serializable {

    private String rota;
    private Double custo;

    public String getRota() {
        return rota;
    }

    public MenorRotaDTO setRota(String rota) {
        this.rota = rota;
        return this;
    }

    public Double getCusto() {
        return custo;
    }

    public MenorRotaDTO setCusto(Double custo) {
        this.custo = custo;
        return this;
    }
}
