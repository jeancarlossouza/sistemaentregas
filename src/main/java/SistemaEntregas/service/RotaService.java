package SistemaEntregas.service;

import SistemaEntregas.model.RotaDTO;
import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class RotaService {

    private String arquivo = new File(".").getCanonicalPath().concat("/BD.json");

    public RotaService() throws IOException {
    }

    public List<RotaDTO> salvar(List<RotaDTO> rotaDTOList) throws Exception {

        List<RotaDTO> rotas = new ArrayList<>();

        try {
            File arquivoBD = new File(arquivo);

            if (arquivoBD.exists()) {
                rotas = carregar();
            }

            rotas.addAll(rotaDTOList);

            FileWriter fileWriter = new FileWriter(arquivo);

            Gson gson = new Gson();
            fileWriter.write(gson.toJson(rotas));
            fileWriter.close();

        } catch (IOException e) {
            throw new Exception("Erro ao gravar rota");
        }
        return rotas;
    }

    public List<RotaDTO> carregar() throws Exception {

        List<RotaDTO> rotas = new ArrayList<>();

        try {
            Gson gson = new Gson();
            ArrayList registro = null;
            registro = (gson.fromJson(new FileReader(arquivo), ArrayList.class));

            for (int i = 0; i < registro.size(); i++) {
                RotaDTO rota = new RotaDTO();
                rota.setOrigem(String.valueOf(((LinkedTreeMap) registro.get(i)).get("origem")));
                rota.setDestino(String.valueOf(((LinkedTreeMap) registro.get(i)).get("destino")));
                rota.setDistancia(Double.valueOf(String.valueOf(((LinkedTreeMap) registro.get(i)).get("distancia"))));
                rotas.add(rota);
            }
        } catch (Exception e) {
            throw new Exception("Arquivo não encontrado");
        }
        return rotas;
    }
}