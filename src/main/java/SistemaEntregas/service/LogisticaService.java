package SistemaEntregas.service;

import SistemaEntregas.model.Descolacamento;
import SistemaEntregas.model.FiltroLogisticaDTO;
import SistemaEntregas.model.MenorRotaDTO;
import SistemaEntregas.model.RotaDTO;
import SistemaEntregas.model.Vertice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;

@Service
public class LogisticaService {

    @Autowired
    private RotaService rotaService;

    private static void analisarRota(Vertice source) {
        source.distancia = 0.;
        PriorityQueue<Vertice> verticeQueue = new PriorityQueue<Vertice>();
        verticeQueue.add(source);

        while (!verticeQueue.isEmpty()) {
            Vertice u = verticeQueue.poll();

            for (Descolacamento e : u.adjacentes) {
                Vertice v = e.destino;
                double distancia = e.distancia;
                double distanceAdjacente = u.distancia + distancia;
                if (distanceAdjacente < v.distancia) {
                    verticeQueue.remove(v);

                    v.distancia = distanceAdjacente;
                    v.previous = u;
                    verticeQueue.add(v);
                }
            }
        }
    }

    public static String buscarMenorDistancia(Vertice destino) {
        List<Vertice> path = new ArrayList<Vertice>();
        for (Vertice vertice = destino; vertice != null; vertice = vertice.previous) {
            path.add(vertice);
        }

        Collections.reverse(path);

        return path.toString();
    }

    private Vertice buscarVertice(List<Map<String, Vertice>> listaVertice, String vertice) {

        Vertice verticeDestino = null;

        for (Map<String, Vertice> map : listaVertice) {
            if (map.containsKey(vertice)) {
                verticeDestino = map.get(vertice);
            }
        }
        return verticeDestino;
    }

    public MenorRotaDTO calcularRotas(FiltroLogisticaDTO filtro) throws Exception {

        List<RotaDTO> rotas = rotaService.carregar();

        Set<String> verticeCriado = new HashSet<>();
        List<Map<String, Vertice>> listaVertice = new ArrayList<>();
        rotas.forEach(rotaDTO -> {
            if (!verticeCriado.contains(rotaDTO.getOrigem())) {
                Map<String, Vertice> mapVerticeOrigem = new HashMap<>();
                mapVerticeOrigem.put(rotaDTO.getOrigem(), new Vertice(rotaDTO.getOrigem()));
                listaVertice.add(mapVerticeOrigem);
                verticeCriado.add(rotaDTO.getOrigem());
            }

            if (!verticeCriado.contains(rotaDTO.getDestino())) {
                Map<String, Vertice> mapVerticeDestino = new HashMap<>();
                mapVerticeDestino.put(rotaDTO.getDestino(), new Vertice(rotaDTO.getDestino()));
                listaVertice.add(mapVerticeDestino);
                verticeCriado.add(rotaDTO.getDestino());
            }
        });

        listaVertice.forEach(vertice -> {
            String key = "";
            List<Descolacamento> descolacamentos = new ArrayList<>();
            for (int i = 0; i < rotas.size(); i++) {
                if (vertice.containsKey(rotas.get(i).getOrigem())) {
                    key = rotas.get(i).getOrigem();
                    descolacamentos.add(new Descolacamento(buscarVertice(listaVertice,
                            rotas.get(i).getDestino()), rotas.get(i).getDistancia()));
                }
            }
            vertice.get(key).adjacentes = descolacamentos;
        });

        Vertice origem = buscarVertice(listaVertice, filtro.getOrigem());
        Vertice destino = buscarVertice(listaVertice, filtro.getDestino());

        analisarRota(origem);

        if (destino.distancia == Double.POSITIVE_INFINITY) {
            throw new Exception("Não há rota disponível para essas coordenadas");
        }

        MenorRotaDTO menorRotaDTO = new MenorRotaDTO();
        menorRotaDTO.setRota(buscarMenorDistancia(destino));
        menorRotaDTO.setCusto(calcularAutonomia(filtro, destino.distancia));
        return menorRotaDTO;
    }

    private Double calcularAutonomia(FiltroLogisticaDTO filtro, Double distancia) {
        return ((distancia / filtro.getAutonomia()) * filtro.getValorLitro());
    }
}



