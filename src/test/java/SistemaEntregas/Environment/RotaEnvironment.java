package SistemaEntregas.Environment;

import SistemaEntregas.model.RotaDTO;

import java.util.ArrayList;
import java.util.List;

public class RotaEnvironment {

    public static List<RotaDTO> criar() {

        List<RotaDTO> lista = new ArrayList<>();
        RotaDTO rota = new RotaDTO();
        rota.setOrigem("A");
        rota.setDestino("B");
        rota.setDistancia(10D);
        lista.add(rota);

        rota = new RotaDTO();
        rota.setOrigem("A");
        rota.setDestino("C");
        rota.setDistancia(15D);
        lista.add(rota);

        rota = new RotaDTO();
        rota.setOrigem("A");
        rota.setDestino("E");
        rota.setDistancia(5D);
        lista.add(rota);

        rota = new RotaDTO();
        rota.setOrigem("B");
        rota.setDestino("C");
        rota.setDistancia(12D);
        lista.add(rota);

        rota = new RotaDTO();
        rota.setOrigem("C");
        rota.setDestino("D");
        rota.setDistancia(20D);
        lista.add(rota);

        rota = new RotaDTO();
        rota.setOrigem("D");
        rota.setDestino("B");
        rota.setDistancia(50D);
        lista.add(rota);

        rota = new RotaDTO();
        rota.setOrigem("E");
        rota.setDestino("D");
        rota.setDistancia(5D);
        lista.add(rota);

        rota = new RotaDTO();
        rota.setOrigem("C");
        rota.setDestino("E");
        rota.setDistancia(30D);
        lista.add(rota);

        return lista;
    }
}
