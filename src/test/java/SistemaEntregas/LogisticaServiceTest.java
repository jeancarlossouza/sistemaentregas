package SistemaEntregas;

import SistemaEntregas.Environment.RotaEnvironment;
import SistemaEntregas.model.FiltroLogisticaDTO;
import SistemaEntregas.model.MenorRotaDTO;
import SistemaEntregas.service.LogisticaService;
import SistemaEntregas.service.RotaService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LogisticaServiceTest {

	@InjectMocks
	private LogisticaService logisticaService;

	@Mock
	private RotaService rotaService;

	@Test
	public void testCalcularMenorCusto() throws Exception {

		FiltroLogisticaDTO filtro = new FiltroLogisticaDTO();
		filtro.setOrigem("E");
		filtro.setDestino("C");
		filtro.setAutonomia(10D);
		filtro.setValorLitro(3.5D);

		when(rotaService.carregar()).thenReturn(RotaEnvironment.criar());

		MenorRotaDTO menorRotaDTO = logisticaService.calcularRotas(filtro);
		Assert.assertEquals("[E, D, B, C]", menorRotaDTO.getRota());
		Assert.assertEquals(23.45D,menorRotaDTO.getCusto(), 0.0001D);
	}

	@Test
	public void testCalculoMenorCustoRotaNaoEncontrada() throws Exception {

		try {
			FiltroLogisticaDTO filtro = new FiltroLogisticaDTO();
			filtro.setOrigem("E");
			filtro.setDestino("A");
			filtro.setAutonomia(10D);
			filtro.setValorLitro(3.5D);

			when(rotaService.carregar()).thenReturn(RotaEnvironment.criar());
			logisticaService.calcularRotas(filtro);

		} catch(Exception e) {
			Assert.assertEquals("Não há rota disponível para essas coordenadas", e.getMessage());
		}
	}
}
